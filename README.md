This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This is a simple **todo app** build with react-redux hook APIs.

## list of React Redux hooks APIs

1. useSelector()
2. useDispatch()
3. useStore()
